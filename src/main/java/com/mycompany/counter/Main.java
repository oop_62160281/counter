/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.counter;

/**
 *
 * @author ASUS
 */
public class Main {
    public static void main(String[] args) {
        // Reference Type
        // Classs
        Counter counter1 = new Counter();
        Counter counter2 = new Counter();
        Counter counter3 = new Counter();
        counter1.increase(); // counter1.countNumber++;
        counter1.increase();
        counter1.increase();
        counter1.print();
        counter2.print();
        counter2.increase();
        counter2.print();
        counter2.decrease();
        counter1.increase();
        counter3.decrease();
        System.out.println("Print All Counter");
        counter1.print();
        counter2.print();
        counter3.print();
    }
}
